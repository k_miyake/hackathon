gulpコマンド一覧

ローカルのgulpインストール↓
npm i -D gulp

GULP起動は下記コマンドでやること＼_(･ω･`)ｺｺ重要!

npx gulp

ファイル構成

htdocs
|
 --dist
    //圧縮などする気ない者達///
    |--js
    |--img
    ////////////////
    |-- css
        |-- style.css //これを読み込めば全てのスタイルが読み込まれる
    |-- component
        |-- ここに部品のhtmlを作っていく
    |-- index.html //本番用HTML
 --src
    //フロックスの仕様でファイル訳をする
    |--scss
        |-- layout
        |-- object
              |-- component
              |-- project
              |-- utility

        |--style.scss