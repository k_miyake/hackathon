const gulp = require("gulp");
const sass = require("gulp-sass");
var sassGlob = require("gulp-sass-glob");
var browserSync = require('browser-sync').create();

const patch = {
    scss: {
        src: './src/scss/style.scss',
        dist: './dist/css'
    }
}

//sass
function scss() {
    return (
        gulp
        .src(patch.scss.src)
        .pipe(sassGlob())
        .pipe(sass())
        .pipe(gulp.dest(patch.scss.dist))
    );
};


//ブラウザー
function buildserver(done) {
    browserSync.init({
        server: {
            baseDir: "./dist",
            index: "index.html"
        }
    });
    done();
}

function reload(done) {
    browserSync.reload();
    done();
}
//監視
function watchFiles(done) {
    gulp.watch('./dist/*.html', gulp.series(reload));
    gulp.watch("./src/scss/**/*.scss", gulp.series(reload,scss));
    done();
};

gulp.task('default', gulp.parallel(scss, watchFiles, buildserver));
